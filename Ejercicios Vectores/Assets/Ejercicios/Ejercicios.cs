﻿using System.Collections;
using System.Collections.Generic;
using MathDebbuger;
using UnityEngine;

public class Ejercicios : MonoBehaviour
{
    public Vector3 Vector_A;
    public Vector3 Vector_B;
    Vector3 Vector_C;

    public Color VectorColour;

    public enum Ejercicio { Uno,Dos,Tres,Cuatro,Cinco,Seis,Siete,Ocho,Nueve,Diez};
    public Ejercicio ejercicios;

    float aux = 0;

    private void Start()
    {
        Vector3Debugger.AddVector(Vector_A, "A");
        Vector3Debugger.AddVector(Vector_B, "B");
        Vector3Debugger.AddVector(Vector_C, "C");
        Vector3Debugger.EnableEditorView();
    }
    private void Update()
    {      
        switch (ejercicios)
        {
            case Ejercicio.Uno:
                Vector_C = Vector_A + Vector_B;
                break;
            case Ejercicio.Dos:
                Vector_C = Vector_B - Vector_A;
                break;
            case Ejercicio.Tres:
                Vector_C.x = Vector_A.x * Vector_B.x;
                Vector_C.y = Vector_A.y * Vector_B.y;
                Vector_C.z = Vector_A.z * Vector_B.z;
                break;
            case Ejercicio.Cuatro:
                Vector_C = Vector3.Cross(Vector_A, Vector_B);
                break;
            case Ejercicio.Cinco:
                aux += Time.deltaTime;
                Vector_C = Vector3.Lerp(Vector_A, Vector_B, aux);
                if (aux > 1) { aux = 0; }
                break;
            case Ejercicio.Seis:
                Vector3.Max(Vector_A, Vector_B);
                break;
            case Ejercicio.Siete:
                Vector_C = Vector3.Project(Vector_A, Vector_B);
                break;
            case Ejercicio.Ocho:
                Vector_C = (Vector_A + Vector_B).normalized * Vector3.Distance(Vector_A, Vector_B);
                break;
            case Ejercicio.Nueve:
                Vector_C = Vector3.Reflect(Vector_A, Vector_B).normalized;
                break;
            case Ejercicio.Diez:
                aux += Time.deltaTime;
                Vector_C = Vector3.LerpUnclamped(Vector_A, Vector_B, aux);
                if (aux > 1) { aux = 0; }
                break;
        }
        Vector3Debugger.UpdatePosition("C", Vector_C);
        Vector3Debugger.UpdatePosition("B", Vector_B);
        Vector3Debugger.UpdatePosition("A", Vector_A);

        Vector3Debugger.UpdateColor("C", VectorColour);
        Vector3Debugger.UpdateColor("B", Color.white);
        Vector3Debugger.UpdateColor("A", Color.black);
    }
}
