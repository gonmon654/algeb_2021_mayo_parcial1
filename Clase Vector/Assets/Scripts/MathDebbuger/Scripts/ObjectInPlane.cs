﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomMath;

public class ObjectInPlane : MonoBehaviour
{
    //Create Cube
    const short cubeSides = 6;
    public PlaneClass[] sides = new PlaneClass[cubeSides];


    //Render Object
    bool insideCube;
    public GameObject objectToAnalyze;

    private void Start()
    {
        //Setting front and back Planes 
        sides[0].SetNormalAndPosition(Vec3.Forward, new Vec3(0, 0, -3));
        sides[1].SetNormalAndPosition(Vec3.Back, new Vec3(0, 0, 3));

        //Setting left and right Planes
        sides[2].SetNormalAndPosition(Vec3.Right, new Vec3(-3, 0, 0));
        sides[3].SetNormalAndPosition(Vec3.Left, new Vec3(3, 0, 0));

        //Setting up and down Planes
        sides[4].SetNormalAndPosition(Vec3.Up, new Vec3(0, -3, 0));
        sides[5].SetNormalAndPosition(Vec3.Down, new Vec3(0, 3, 0));
    }

    private void Update()
    {
        insideCube = true;
        for(short i = 0; i < cubeSides; i++)
        {
            if (!sides[i].GetSide(objectToAnalyze.transform.position))
            {
                insideCube = false;
                Debug.Log("No Esta en el Centro");
            }
        }
        objectToAnalyze.GetComponent<MeshRenderer>().enabled = insideCube;
    }
}
