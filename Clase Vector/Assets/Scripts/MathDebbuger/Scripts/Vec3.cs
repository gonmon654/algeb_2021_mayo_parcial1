﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace CustomMath
{
    public struct Vec3 : IEquatable<Vec3>
    {
        #region Variables
        public float x;
        public float y;
        public float z;

        public float sqrMagnitude { get {return SqrMagnitude(this);} }
        public Vec3 normalized { get { return this.normalized; } }
        public float magnitude { get {return (Mathf.Sqrt(sqrMagnitude)); } }
        #endregion

        #region constants
        public const float epsilon = 1e-05f;
        #endregion

        #region Default Values
        public static Vec3 Zero { get { return new Vec3(0.0f, 0.0f, 0.0f); } }
        public static Vec3 One { get { return new Vec3(1.0f, 1.0f, 1.0f); } }
        public static Vec3 Forward { get { return new Vec3(0.0f, 0.0f, 1.0f); } }
        public static Vec3 Back { get { return new Vec3(0.0f, 0.0f, -1.0f); } }
        public static Vec3 Right { get { return new Vec3(1.0f, 0.0f, 0.0f); } }
        public static Vec3 Left { get { return new Vec3(-1.0f, 0.0f, 0.0f); } }
        public static Vec3 Up { get { return new Vec3(0.0f, 1.0f, 0.0f); } }
        public static Vec3 Down { get { return new Vec3(0.0f, -1.0f, 0.0f); } }
        public static Vec3 PositiveInfinity { get { return new Vec3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity); } }
        public static Vec3 NegativeInfinity { get { return new Vec3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity); } }
        #endregion                                                                                                                                                                               

        #region Constructors
        public Vec3(float x, float y)
        {
            this.x = x;
            this.y = y;
            this.z = 0.0f;
        }

        public Vec3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vec3(Vec3 v3)
        {
            this.x = v3.x;
            this.y = v3.y;
            this.z = v3.z;
        }

        public Vec3(Vector3 v3)
        {
            this.x = v3.x;
            this.y = v3.y;
            this.z = v3.z;
        }

        public Vec3(Vector2 v2)
        {
            this.x = v2.x;
            this.y = v2.y;
            this.z = 0.0f;
        }
        #endregion

        #region Operators
        public static bool operator ==(Vec3 left, Vec3 right)
        {
            float diff_x = left.x - right.x;
            float diff_y = left.y - right.y;
            float diff_z = left.z - right.z;
            float sqrmag = diff_x * diff_x + diff_y * diff_y + diff_z * diff_z;
            return sqrmag < epsilon * epsilon;
        }
        public static bool operator !=(Vec3 left, Vec3 right)
        {
            return !(left == right);
        }

        public static Vec3 operator +(Vec3 leftV3, Vec3 rightV3)
        {
            return new Vec3(leftV3.x + rightV3.x, leftV3.y + rightV3.y, leftV3.z + rightV3.z);
        }

        public static Vec3 operator -(Vec3 leftV3, Vec3 rightV3)
        {
            return new Vec3(leftV3.x - rightV3.x, leftV3.y - rightV3.y, leftV3.z - rightV3.z);
        }

        public static Vec3 operator -(Vec3 v3)
        {
            return new Vec3((-v3.x), (-v3.y), (-v3.z));
        }
        public static Vec3 operator *(Vec3 v3, float scalar)
        {
            return new Vec3(v3.x * scalar, v3.y * scalar, v3.z * scalar);
        }
        public static Vec3 operator *(float scalar, Vec3 v3)
        {
            return new Vec3(v3.x * scalar, v3.y * scalar, v3.z * scalar);
        }
        public static Vec3 operator /(Vec3 v3, float scalar)
        {
            return new Vec3(v3.x / scalar, v3.y / scalar, v3.z / scalar);
        }

        public static implicit operator Vector3(Vec3 v3)
        {
            return new Vector3(v3.x, v3.y, v3.z);
        }

        public static implicit operator Vector2(Vec3 v2)
        {
            return new Vector2(v2.x, v2.y);
        }

        public static implicit operator Vec3(Vector3 v3)
        {
            return new Vec3(v3.x, v3.y, v3.z);
        }
        #endregion

        #region Functions
        public override string ToString()
        {
            return "X = " + x.ToString() + "   Y = " + y.ToString() + "   Z = " + z.ToString();
        }
        public static float Angle(Vec3 from, Vec3 to)
        {
            return Mathf.Rad2Deg * Mathf.Acos(Dot(from,to)) / (Magnitude(from) * Magnitude(to));
        }
        public static Vec3 ClampMagnitude(Vec3 vector, float maxLength)
        {
            if (Magnitude(vector) > maxLength)
            {
                return vector / maxLength;
            }
            else
            {
                return vector;
            }
        }
        public static float Magnitude(Vec3 vector)
        {
            return Mathf.Sqrt(SqrMagnitude(vector));
        }
        public static Vec3 Cross(Vec3 a, Vec3 b)
        {
            Vec3 vector;
            vector.x = (a.y * b.z) - (a.z * b.y);
            vector.y = (a.z * b.x) - (a.x * b.z);
            vector.z = (a.x * b.y) - (a.y * b.x); 
            //A por B da en un sentido, B por A en otro
            return vector;
        }
        public static float Distance(Vec3 a, Vec3 b)
        {
            return Mathf.Sqrt(Mathf.Pow(a.x - b.x, 2) + Mathf.Pow(a.y - b.y, 2) + Mathf.Pow(a.z - b.z, 2));
        }
        public static float Dot(Vec3 a, Vec3 b)
        {
            return (a.x * b.x) + (a.y + b.y) + (a.z * b.z);
        }
        public static Vec3 Lerp(Vec3 a, Vec3 b, float t)
        {
            if (t < 0)
            {
                return b;
            }
            else if (t > 1)
            {
                return a;
            }
            else
            {
                return t * a + (1 - t) * b;
            }
        }
        public static Vec3 LerpUnclamped(Vec3 a, Vec3 b, float t)
        {
            return t * a + (1 - t) * b;
        }
        public static Vec3 Max(Vec3 a, Vec3 b)
        {
            Vec3 vectorK;
            if(a.x>b.x)      
                vectorK.x = a.x;           
            else
                vectorK.x = b.x;
            
            if (a.y > b.y) 
                vectorK.y = a.y; 
            else
                vectorK.y = b.y;     
            
            if (a.z > b.z)
                vectorK.z = a.z;
            else
                vectorK.z = b.z;

            return vectorK;
        }
        public static Vec3 Min(Vec3 a, Vec3 b)
        {
            Vec3 vectorK;
            if (a.x > b.x)
                vectorK.x = b.x;
            else
                vectorK.x = a.x;

            if (a.y > b.y)
                vectorK.y = b.y;
            else
                vectorK.y = a.y;

            if (a.z > b.z)
                vectorK.z = b.z;
            else
                vectorK.z = a.z;

            return vectorK;
        }
        public static float SqrMagnitude(Vec3 vector)
        {
            return Mathf.Pow(vector.x, 2) + Mathf.Pow(vector.y, 2) + Mathf.Pow(vector.z, 2);
        }
        public static Vec3 Project(Vec3 vector, Vec3 onNormal) 
        {
            return onNormal * (Dot(vector, onNormal) / (SqrMagnitude(onNormal)));
        }
        public static Vec3 Reflect(Vec3 inDirection, Vec3 inNormal) 
        {
            float d = 2 * Dot(inDirection, inNormal);
            return inDirection - inNormal * d;
        }
        public void Set(float newX, float newY, float newZ)
        {
            x = newX;
            y = newY;
            z = newZ;
        }
        public void Scale(Vec3 scale)
        {
            x *= scale.x;
            y *= scale.y;
            z *= scale.z;
        }
        public void Normalize()
        {
            x /= magnitude;
            y /= magnitude;
            z /= magnitude;
        }
        public static Vec3 Normalize(Vec3 vec)
        {
            return vec / Magnitude(vec);
        }
        public void ChangeFromVector(Vector3 originalVector)
        {
            this.x = originalVector.x;
            this.y = originalVector.y;
            this.z = originalVector.z;
        }
        #endregion

        #region Internals
        public override bool Equals(object other)
        {
            if (!(other is Vec3)) return false;
            return Equals((Vec3)other);
        }

        public bool Equals(Vec3 other)
        {
            return x == other.x && y == other.y && z == other.z;
        }

        public override int GetHashCode()
        {
            return x.GetHashCode() ^ (y.GetHashCode() << 2) ^ (z.GetHashCode() >> 2);
        }
        #endregion
    }
}