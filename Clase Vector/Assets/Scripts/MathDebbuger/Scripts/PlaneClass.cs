﻿using System;
using UnityEngine;
using CustomMath;

namespace CustomMath
{
    [System.Serializable]
    public struct PlaneClass
    {
        #region Variables
        public Vector3 normal { get; set; }
        public PlaneClass flipped { get { return new PlaneClass(-normal, -distance); } }
        public float distance { get; set; }
        #endregion

        #region constants
        public const float epsilon = 1e-05f;
        #endregion

        #region Constructors
        public PlaneClass(Vector3 inNormal, Vector3 inPoint)
        {
            normal = inNormal.normalized;
            distance = -Vector3.Dot(normal, inPoint);
        }

        public PlaneClass(Vector3 inNormal, float d)
        {
            normal = Vector3.Normalize(inNormal);
            distance = d;
        }

        public PlaneClass(Vector3 a, Vector3 b, Vector3 c)
        {
            Vector3 auxVector = Vector3.Cross(b - a, a - c);
            normal = auxVector.normalized;
            distance = -Vector3.Dot(normal, a);
        }
        #endregion

        #region Functions
        public override string ToString()
        {
            return "normal = " + normal.ToString() +
                "   distance = " + distance.ToString() +
                "   flipped = " + flipped.ToString();
        }
        public Vector3 ClosestPointOnPlane(Vector3 point)
        {
            return point - normal * GetDistanceToPoint(point);
        }
        public void Flip()
        {
            normal = - normal;
            distance = -distance;
        }
        public float GetDistanceToPoint(Vec3 point)
        {
            return Vec3.Dot(normal, point) + distance;
        }
        public bool GetSide(Vector3 point)
        {
            return GetDistanceToPoint(point) > -epsilon;
        }
        public bool Raycast(Ray ray, out float enter) //No hacer
        {
            throw new NotImplementedException();
        }
        public bool SameSide(Vector3 inPt0, Vector3 inPt1)
        {
            float distanceToPoint1 = GetDistanceToPoint(inPt0);
            float distanceToPoint2 = GetDistanceToPoint(inPt1);
            return distanceToPoint1 > 0.0 && distanceToPoint2 > 0.0 || distanceToPoint1 <= 0.0 && distanceToPoint2 <= 0.0;
        }
        public void Set3Points(Vector3 a, Vector3 b, Vector3 c)
        {
            normal = Vector3.Normalize(Vector3.Cross(b - a, c - a));
            distance = -Vector3.Dot(normal, a);
        }
        public void SetNormalAndPosition(Vector3 inNormal, Vector3 inPoint)
        {
            normal = Vector3.Normalize(inNormal);
            distance = -Vector3.Dot(inNormal, inPoint);
        }
        public void Translate(Vector3 translation)
        {
            distance += Vector3.Dot(normal, translation);
        }
        public static PlaneClass Translate(PlaneClass plane, Vec3 translation)
        {
            return new PlaneClass(plane.normal, plane.distance += Vector3.Dot(plane.normal, translation)); //Generamos la copia con los nuevos valores
        }
        #endregion
    }
}