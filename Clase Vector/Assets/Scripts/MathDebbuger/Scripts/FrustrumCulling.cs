﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CustomMath;

public class FrustrumCulling : MonoBehaviour
{
    //Call camera
    private Camera mainCamera;

    //Mesh Renderer List
    public List<GameObject> objectsForFrustrum = null;

    //Cube List
    public List<GameObject> listCubos = null;

    //Position Variables
    Vector3 position;
    Vector3 direction;
    Vector3 up;
    Vector3 right;

    //Distance for FOV
    float nearDist;
    float farDist;

    //Height and Width for near
    float Hnear;
    float Wnear;

    //Height and Width for far
    float Hfar;
    float Wfar;

    //Points
    const int pointsNearFar = 4; //Cantidad de puntos en cada plano (cercano y lejano)
    Vector3[] nearPoints = new Vector3[4]; //Puntos de plano cercano (va en el orden: arriba derecha, arriba izquierda, abajo derecha, abajo izquierda)
    Vector3[] farPoints = new Vector3[4]; //Puntos de plano lejano (va en el orden: arriba derecha, arriba izquierda, abajo derecha, abajo izquierda)

    //Centros
    Vector3 nc, fc;
    public Vector3 aux;

    //Planos
    const short sides = 6;
    Plane[] frustrum = new Plane[sides];

    private void Awake()
    {
        mainCamera = Camera.main;
        CheckGameObjects();
        DebugStart();
    }

    void Update()
    {
        nearDist = mainCamera.nearClipPlane;
        farDist = mainCamera.farClipPlane;

        up = mainCamera.transform.up;
        right = mainCamera.transform.right;

        position = mainCamera.transform.position;
        direction = mainCamera.transform.forward;

        Hnear = (Mathf.Tan(Mathf.Deg2Rad * (mainCamera.fieldOfView / 2)) * mainCamera.nearClipPlane);
        Wnear = Hnear * mainCamera.aspect;

        Hfar = (Mathf.Tan(Mathf.Deg2Rad * (mainCamera.fieldOfView / 2)) * mainCamera.farClipPlane);
        Wfar = Hfar * mainCamera.aspect;

        //Hago update a los puntos y a los gameobjects en juego
        UpdateCenters();
        UpdateNearPoints();
        UpdateFarPoints();
        UpdatePlanes();
        UpdateCubes();

        //Reviso si los vertices de un objeto estan adentro del frustrum
        for (short i = 0; i < objectsForFrustrum.Count; i++)
        {
            if (CheckMeshRendererPoints(i))
            {
                objectsForFrustrum[i].gameObject.GetComponent<MeshRenderer>().enabled = true;
            }
            else
            {
                objectsForFrustrum[i].gameObject.GetComponent<MeshRenderer>().enabled = false;
            }
        }
    }

    void UpdateCenters()
    {
        fc.x = position.x + direction.x * farDist;
        fc.y = position.y + direction.y * farDist;
        fc.z = position.z + direction.z * farDist;

        nc.x = position.x + direction.x * nearDist;
        nc.y = position.y + direction.y * nearDist;
        nc.z = position.z + direction.z * nearDist;
    }

    void UpdateNearPoints()
    {
        nearPoints[0].x = nc.x + (up.x * Hnear) + (right.x * Wnear);
        nearPoints[0].y = nc.y + (up.y * Hnear) + (right.y * Wnear);
        nearPoints[0].z = nc.z + (up.z * Hnear) + (right.z * Wnear);

        nearPoints[1].x = nc.x + (up.x * Hnear) - (right.x * Wnear);
        nearPoints[1].y = nc.y + (up.y * Hnear) - (right.y * Wnear);
        nearPoints[1].z = nc.z + (up.z * Hnear) - (right.z * Wnear);

        nearPoints[2].x = nc.x - (up.x * Hnear) + (right.x * Wnear);
        nearPoints[2].y = nc.y - (up.y * Hnear) + (right.y * Wnear);
        nearPoints[2].z = nc.z - (up.z * Hnear) + (right.z * Wnear);

        nearPoints[3].x = nc.x - (up.x * Hnear) - (right.x * Wnear);
        nearPoints[3].y = nc.y - (up.y * Hnear) - (right.y * Wnear);
        nearPoints[3].z = nc.z - (up.z * Hnear) - (right.z * Wnear);
    }

    void UpdateFarPoints()
    {
        farPoints[0].x = fc.x + (up.x * Hfar) + (right.x * Wfar);
        farPoints[0].y = fc.y + (up.y * Hfar) + (right.y * Wfar);
        farPoints[0].z = fc.z + (up.z * Hfar) + (right.z * Wfar);

        farPoints[1].x = fc.x + (up.x * Hfar) - (right.x * Wfar);
        farPoints[1].y = fc.y + (up.y * Hfar) - (right.y * Wfar);
        farPoints[1].z = fc.z + (up.z * Hfar) - (right.z * Wfar);

        farPoints[2].x = fc.x - (up.x * Hfar) + (right.x * Wfar);
        farPoints[2].y = fc.y - (up.y * Hfar) + (right.y * Wfar);
        farPoints[2].z = fc.z - (up.z * Hfar) + (right.z * Wfar);

        farPoints[3].x = fc.x - (up.x * Hfar) - (right.x * Wfar);
        farPoints[3].y = fc.y - (up.y * Hfar) - (right.y * Wfar);
        farPoints[3].z = fc.z - (up.z * Hfar) - (right.z * Wfar);
    }

    void UpdatePlanes()
    {
        frustrum[0].Set3Points(nearPoints[0], nearPoints[1], nearPoints[3]); //Near Plane
        frustrum[1].Set3Points(farPoints[0], farPoints[1], farPoints[3]); //Far Plane


        frustrum[2].Set3Points(position, farPoints[3], farPoints[1]); //Left Plane
        frustrum[3].Set3Points(position, farPoints[0], farPoints[2]); //Right Plane
        frustrum[4].Set3Points(position, farPoints[1], farPoints[0]); //Top Plane

        frustrum[5] = new Plane(farPoints[2], farPoints[3], position); //Bottom Plane

        Vector3 center = transform.position + transform.forward * ((mainCamera.farClipPlane + mainCamera.nearClipPlane) / 2);
        aux = center;

        frustrum[0].normal = (nc - center).normalized;
        frustrum[1].normal = (fc - center).normalized;
        

        for (int i = 0; i < sides; i++)
        {
            if (!frustrum[i].SameSide(frustrum[i].normal, center))
            {
               frustrum[i].Flip();        
            }
            bool aux = frustrum[i].SameSide(frustrum[i].normal, objectsForFrustrum[0].transform.position);
        }
    }

    void CheckGameObjects()
    {
        foreach (MeshRenderer subject in FindObjectsOfType<MeshRenderer>())
        {
            if (subject.gameObject.tag != "Plane")
            {
                objectsForFrustrum.Add(subject.gameObject);
            }          
        }
    }

    bool CheckMeshRendererPoints(short i)
    {
        int isInsideSide = 0;
        Vector3[] objectVertices = objectsForFrustrum[i].GetComponent<MeshFilter>().mesh.vertices;
        foreach (Vector3 vertices in objectVertices)
        {
            isInsideSide = 0;
            Vector3 vertexGlobalPosition = objectsForFrustrum[i].transform.TransformPoint(vertices);
            for (short j = 0; j < sides; j++)
            {
                if (j == 5)
                {
                    isInsideSide++;
                    continue;
                }
                if (frustrum[j].SameSide(vertexGlobalPosition, frustrum[j].normal))
                {
                    isInsideSide++;
                }
                else
                {
                    Debug.Log(j);
                }
            }
            if (isInsideSide == 6)
            {
                return true;
            }
        }
        return false;
    }

    void DebugStart()
    {
        //for (int i = 0; i < 9; i++)
        //{
        //    listCubos.Add(GameObject.CreatePrimitive(PrimitiveType.Cube));
        //    listCubos[i].transform.parent = transform;
        //}
        //listCubos[0].name = "ntl";
        //listCubos[1].name = "ntr";
        //listCubos[2].name = "nbl";
        //listCubos[3].name = "nbr";
        //listCubos[4].name = "ftl";
        //listCubos[5].name = "ftr";
        //listCubos[6].name = "fbl";
        //listCubos[7].name = "fbr";
        //listCubos[8].name = "center";
    }

    void UpdateCubes()
    {
        //listCubos[0].transform.position = nearPoints[1];
        //listCubos[1].transform.position = nearPoints[0];
        //listCubos[2].transform.position = nearPoints[3];
        //listCubos[3].transform.position = nearPoints[2];

        //listCubos[4].transform.position = farPoints[1];
        //listCubos[5].transform.position = farPoints[0];
        //listCubos[6].transform.position = farPoints[3];
        //listCubos[7].transform.position = farPoints[2];

        //listCubos[8].transform.position = transform.position + transform.forward * ((mainCamera.farClipPlane + mainCamera.nearClipPlane) / 2);

        //listCubos[0].name = "ntl near 1";

        //listCubos[1].name = "ntr near 0";

        //listCubos[2].name = "nbl near 3";

        //listCubos[3].name = "nbr near 2";

        //listCubos[4].name = "ftl far 1";

        //listCubos[5].name = "ftr far 0";

        //listCubos[6].name = "fbl far 3";

        //listCubos[7].name = "fbr far 2";

        //listCubos[8].name = "center";
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(aux, 1);
    }
}
